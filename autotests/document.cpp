// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#undef QT_NO_CAST_FROM_ASCII // for QStringList

#include <QSignalSpy>
#include <QTest>
#include <QTextBlock>

#include "geminidocument.h"

class Document : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testFormat();
    void testSingleLine();

    void testPartial_data();
    void testPartial();

    void testLineEndings();

    void testNullByte();
    void testNullLink();

    void testTitle();

    void preformat();
    void testQuote();
    void testList();
    void testLink();
    void testEmptyLine();

    void testInvalidList();
    void testListQuote();

    void testClear();

private:
    const QUrl url{QStringLiteral("file://foo")};
};

void Document::testFormat()
{
    const QString source = QStringLiteral("foo\nbar");
    // no newline for last line since toPlainText() does not generate one

    GeminiDocument document;
    document.setSource(source, url);

    QCOMPARE(document.source(), source);
    QCOMPARE(document.toPlainText(), source);
}

void Document::testSingleLine()
{
    const QString source = QStringLiteral("foo");

    GeminiDocument document;
    document.setSource(source, url);

    QCOMPARE(document.toPlainText(), source);
}

void Document::testNullByte()
{
    const QString source = QString::fromUtf8("foo\0bar\nbaz\n", 12);

    GeminiDocument document;
    document.setSource(source, url);

    QCOMPARE(document.toPlainText() + '\n', source);
}

void Document::testNullLink()
{
    const QString source = QString::fromUtf8("=> hxxp://x\0/ foo\n", 18);

    GeminiDocument document;
    document.setSource(source, url);

    QCOMPARE(document.toMarkdown(), "foo\n\n");
}

void Document::testPartial_data()
{
    QTest::addColumn<QStringList>("sourceParts");

    QTest::newRow("at linebreak") << QStringList({"asdf\n", "bar"});
    QTest::newRow("over linebreak") << QStringList({"asdf", "ba\nr\n"});
    QTest::newRow("headings") << QStringList({"# FOO\n", "#", "#", "# bar\n"});
    QTest::newRow("list") << QStringList({"foo b", "ar\n* ", "a\n* b\n", "* c\n"});
    QTest::newRow("link") << QStringList({"=", "> ", "gmi://local ", " go"});
    QTest::newRow("preformat") << QStringList({"``", "`cpp", "\nvoid main();\n`", "``\nhello\n"});
    QTest::newRow("first line") << QStringList({"as", "d", "f\n"});

    QTest::newRow("list2") << QStringList({
        "A\n\n* A",
        "a",
    });
    QTest::newRow("list3") << QStringList({
        " Test\ntp://asdf> B\na\n* A\n\n```\n",
        "",
    });

    //
    // Some differences found with fuzzing. The appendSource method used then is
    // no longer used, because not all edgecases could be fixed due to
    // problemeatic interactions with QTextDocument.
    //
    QTest::newRow("fuzz1") << QStringList({
        " Test\n=> ht|hhhhhhhhhhp://asdf\n```a",
        "\n* A\n> B\n",
    });
    QTest::newRow("fuzz2") << QStringList({
        "***************\n=> htbp://asd|\n```",
        "\n",
    });

    QTest::newRow("fuzz3") << QStringList({"#> f\n```aq:f```af\n```a\n*", " A\n"});

    QTest::newRow("fuzz4") << QStringList({QString::fromUtf8("t\n=> h://\0\n```a\nB", 18), "\n"});
    QTest::newRow("fuzz5") << QStringList({QString::fromUtf8(" Tert\n=> http://as\0\0\0\n```a\nC A\u0006Z`B", 35), "\n"});
}

void Document::testPartial()
{
    GeminiDocument document;

    document.setUrl(QUrl(QStringLiteral("file://foo")));

    QFETCH(QStringList, sourceParts);

    QString source;
    for (int i = 0; i < sourceParts.size(); i++) {
        source.append(sourceParts[i]);
        document.appendSource(sourceParts[i]);
    }
    document.endAppendSource();

    QCOMPARE(document.source(), source);

    GeminiDocument docComplete;
    docComplete.setSource(source, url);
    QCOMPARE(docComplete.source(), source);
    QCOMPARE(document.toPlainText(), docComplete.toPlainText());
    QCOMPARE(document.toMarkdown(), docComplete.toMarkdown());
    QCOMPARE(document.toHtml(), docComplete.toHtml());
}

void Document::testLineEndings()
{
    GeminiDocument docUnix;
    GeminiDocument docDos;

    docUnix.setSource(QStringLiteral("foo\nbar\nbaz\n"), url);
    docDos.setSource(QStringLiteral("foo\r\nbar\r\nbaz\r\n"), url);

    QCOMPARE(docDos.toPlainText(), docUnix.toPlainText());

    GeminiDocument docMixed;
    docMixed.setSource(QStringLiteral("foo\nbar\r\nbaz\n"), url);
    QCOMPARE(docMixed.toPlainText(), docUnix.toPlainText());
}

void Document::testTitle()
{
    const QString source = QStringLiteral("# some title\nsome text");

    GeminiDocument document;
    QVERIFY(document.metaInformation(QTextDocument::MetaInformation::DocumentTitle).isEmpty());
    QSignalSpy titleSpy(&document, &GeminiDocument::titleChanged);

    document.setSource(source, url);

    QCOMPARE(titleSpy.count(), 1);
    QCOMPARE(titleSpy.takeFirst().at(0), QStringLiteral("some title"));
    QCOMPARE(document.metaInformation(QTextDocument::MetaInformation::DocumentTitle), QStringLiteral("some title"));
}

void Document::preformat()
{
    const QString rawText = QStringLiteral("foo\nbar\n# baz\n");
    const QString source = QStringLiteral("```\n") + rawText + QStringLiteral("```\n");

    GeminiDocument document;
    document.setSource(source, url);

    QCOMPARE(document.toPlainText(), rawText);

    const QString mdSource = source + '\n';

    QCOMPARE(document.toMarkdown(), mdSource);
}

void Document::testQuote()
{
    const QString source = QStringLiteral("No Quote\n>Quote\n> Quote2\nnoq\n");
    auto plain = source;
    plain.remove(QLatin1Char('>'));
    const QString markdown = QStringLiteral("No Quote\n\n> Quote\n\n>  Quote2\n\nnoq\n\n");

    GeminiDocument document;
    document.setSource(source, url);

    QCOMPARE(document.toPlainText() + QLatin1Char('\n'), plain);
    // QCOMPARE(document.toMarkdown(), markdown); // only works with indent 0
}

void Document::testList()
{
    const QString source = QStringLiteral("Line\n* list item 1\n* list item 2\nOther line\n");

    GeminiDocument document;
    document.setSource(source, url);

    const QString plain = QStringLiteral("Line\nlist item 1\nlist item 2\nOther line");
    QCOMPARE(document.toPlainText(), plain);
}

void Document::testLink()
{
    const QString source = QStringLiteral("=> hxxp://local/gemini/ launch\n");

    GeminiDocument document;
    document.setSource(source, url);

    const QString md = QStringLiteral("[launch](hxxp://local/gemini/)\n\n");
    QCOMPARE(document.toMarkdown(), md);
}

void Document::testEmptyLine()
{
    const QString source = QStringLiteral("A\n\nB\n");

    GeminiDocument document;
    document.setSource(source, url);

    const QString plain = QStringLiteral("A\n\nB");
    QCOMPARE(document.toPlainText(), plain);
}

void Document::testInvalidList()
{
    const QString source = QStringLiteral("*\n*\n");

    GeminiDocument document;
    document.setSource(source, url);

    const QString plain = QStringLiteral("*\n*");
    QCOMPARE(document.toPlainText(), plain);
}

void Document::testListQuote()
{
    const QString source = QStringLiteral("*  \n* Q\n> ");

    GeminiDocument document;
    document.setSource(source, url);

    const QString plain = QStringLiteral(" \nQ\n ");
    QCOMPARE(document.toPlainText(), plain);
}

void Document::testClear()
{
    const QString source = QStringLiteral("foo\nbar\n* xx\n");
    const QString plain = QStringLiteral("foo\nbar\nxx");

    GeminiDocument document;
    document.setSource(source, url);

    QCOMPARE(document.toPlainText(), plain);

    const QString md = document.toMarkdown();

    document.clear();

    document.setSource(source, url);
    QCOMPARE(document.toPlainText(), plain);
    QCOMPARE(document.toMarkdown(), md);
}

QTEST_MAIN(Document);

#include "document.moc"
