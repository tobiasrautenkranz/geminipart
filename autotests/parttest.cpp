// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminipart.h"
#include "geminiview.h"

#include <QTest>

#include <KParts/BrowserExtension>
#include <KParts/TextExtension>
#include <KPluginMetaData>
#include <QClipboard>
#include <QJsonDocument>
#include <QScrollBar>
#include <QSignalSpy>

class PartTest : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testSetup();
    void progressiveLoading();
    void textExtension();
    void scroll();
    void select();

private:
    static KPluginMetaData metaData();
};

KPluginMetaData PartTest::metaData()
{
    QJsonObject data = QJsonDocument::fromJson(
                           R"({
    "KPlugin": {
     "Id": "geminipart",
     "Name": "Gemini",
     "Version": "0.1"
    }})")
                           .object();
    return KPluginMetaData(data, QString());
}

void PartTest::testSetup()
{
    GeminiPart part(nullptr, nullptr, PartTest::metaData(), {});

    QSignalSpy spyCompleted(&part, qOverload<>(&KParts::ReadOnlyPart::completed));

    auto *ext = part.browserExtension();
    QSignalSpy spyOpenUrlNotify(ext, &KParts::BrowserExtension::openUrlNotify);

    const QUrl url(QStringLiteral("data:text/gemini,Hello world%0A"));

    part.openUrl(url);

    QVERIFY(spyCompleted.wait());

    QVERIFY(spyOpenUrlNotify.isEmpty());

    QCOMPARE(part.view()->toPlainText(), QStringLiteral("Hello world"));

    QCOMPARE(ext->xOffset(), 0);
    QCOMPARE(ext->yOffset(), 0);
}

void PartTest::progressiveLoading()
{
    GeminiPart part(nullptr, nullptr, PartTest::metaData(), {});
    auto size = part.view()->sizeHint();
    part.view()->resize(size.width(), size.height() / 2);
    auto *ext = part.browserExtension();

    QVERIFY(part.openStream(QStringLiteral("text/gemini"), QUrl(QStringLiteral("gemini://example.com/foo"))));
    QStringList data({QStringLiteral("Hello\n"), QStringLiteral("World\n"), QStringLiteral("gemini\n")});

    // Add some lines to overflow the content
    for (int i = 0; i < 10; i++) {
        data.append(QStringLiteral("foo\n"));
    }

    for (int i = 0; i < data.size(); i++) {
        QVERIFY(part.writeStream(data[i].toUtf8()));
        auto text = data.mid(0, i + 1).join(QString());

        // adjust for missing \n of toPlainText
        if (text.endsWith(QLatin1Char('\n')))
            text.remove(text.size() - 1, 1);
        QCOMPARE(part.view()->document()->toPlainText(), text);
    }
    QVERIFY(part.closeStream());

    part.view()->setVisible(true);

    // can scroll
    QVERIFY(part.view()->verticalScrollBar()->maximum() > part.view()->viewport()->height());

    QCOMPARE(ext->xOffset(), 0);
    QCOMPARE(ext->yOffset(), 0);

    const QString result = part.view()->document()->toPlainText() + QLatin1Char('\n');
    const QString expected = data.join(QString());
    QCOMPARE(result, expected);
}

void PartTest::textExtension()
{
    GeminiPart part(nullptr, nullptr, PartTest::metaData(), {});
    auto textExt = KParts::TextExtension::childObject(&part);

    QVERIFY(textExt);
    QVERIFY(part.openStream(QStringLiteral("text/gemini"), QUrl(QStringLiteral("gemini://example.com/foo"))));
    QString data = QStringLiteral("foobarbazbarfoo");
    QString needle = QStringLiteral("bar");
    QVERIFY(part.writeStream(data.toUtf8()));
    QVERIFY(part.closeStream());

    QCOMPARE(textExt->completeText(KParts::TextExtension::PlainText), data);

    QVERIFY(textExt->findText(needle, KFind::SearchOptions()));
    QCOMPARE(part.view()->textCursor().selectionStart(), data.indexOf(needle));
    QCOMPARE(part.view()->textCursor().selectionEnd(), data.indexOf(needle) + needle.size());

    QVERIFY(textExt->findText(needle, KFind::SearchOptions()));
    QCOMPARE(part.view()->textCursor().selectedText(), needle);
    QVERIFY(part.view()->textCursor().position() > data.indexOf(needle));

    QVERIFY(!textExt->findText(needle, KFind::SearchOptions()));
}

void PartTest::scroll()
{
    GeminiPart part(nullptr, nullptr, PartTest::metaData(), {});
    auto *ext = part.browserExtension();
    QVERIFY(part.openStream(QStringLiteral("text/gemini"), QUrl(QStringLiteral("gemini://example.com/navigate"))));
    QString data = QStringLiteral(R"(# Foo
  asdf
  => data:text/gemini,hello
  ## bar
  * 1
  * 2
  * 3
  )");

    QVERIFY(part.writeStream(data.toUtf8()));
    QVERIFY(part.closeStream());

    auto size = part.view()->sizeHint();
    part.view()->resize(size.width() / 2, size.height() / 3);
    part.view()->setVisible(true);
    auto lines = part.view()->document()->toPlainText().split(QLatin1Char('\n'));
    QCOMPARE(lines.at(0), QStringLiteral("Foo"));

    // can scroll
    QVERIFY(part.view()->verticalScrollBar()->maximum() > part.view()->viewport()->height());

    QCOMPARE(ext->xOffset(), 0);
    QCOMPARE(ext->yOffset(), 0);

    QTest::keyPress(part.view(), Qt::Key_PageDown);
    QCOMPARE(ext->xOffset(), 0);
    QVERIFY(ext->yOffset() > 0);
}

void PartTest::select()
{
    auto clipboard = QGuiApplication::clipboard();
    clipboard->setText(QStringLiteral("NO_TEXT"));
    if (clipboard->supportsSelection()) {
        clipboard->setText(QStringLiteral("NO_TEXT"), QClipboard::Selection);
    }

    GeminiPart part(nullptr, nullptr, PartTest::metaData(), {});
    QVERIFY(part.openStream(QStringLiteral("text/gemini"), QUrl(QStringLiteral("gemini://example.com/navigate"))));
    QString data = QStringLiteral("Foo");

    QVERIFY(part.writeStream(data.toUtf8()));
    QVERIFY(part.closeStream());

    QTest::keyClick(part.view(), 'a', Qt::ControlModifier);
    QCOMPARE(part.view()->textCursor().selectedText(), data);
    if (clipboard->supportsSelection()) {
        QCOMPARE(clipboard->text(QClipboard::Selection), data);
    }

    QTest::keyClick(part.view(), 'c', Qt::ControlModifier);

    QCOMPARE(clipboard->text(QClipboard::Clipboard), data);
}

QTEST_MAIN(PartTest);

#include "parttest.moc"
