set(geminipart_SRCS
  geminidocument.cpp
  geminiview.cpp
  geminipart.cpp
  geminibrowserextension.cpp
  geminitextextension.cpp)

ecm_qt_declare_logging_category(geminipart_SRCS
    IDENTIFIER GEMINIPART
    CATEGORY_NAME geminipart
    HEADER gemini-debug.h
    DESCRIPTION "Gemini KPart"
    EXPORT kpart-gemini
)

ecm_qt_install_logging_categories(
    EXPORT kpart-gemini
    FILE geminipart.categories
    DESTINATION "${KDE_INSTALL_LOGGINGCATEGORIESDIR}"
)

configure_file(geminipart.json.in
  ${CMAKE_CURRENT_BINARY_DIR}/geminipart.json @ONLY)

add_library(geminipartlib OBJECT ${geminipart_SRCS})
target_link_libraries(geminipartlib
  PUBLIC
  Qt5::Core
  KF5::Parts
  KF5::I18n
  Qt5::Widgets)
target_include_directories(geminipartlib PUBLIC "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/>")

add_library(geminipart MODULE geminipartfactory.cpp)
target_link_libraries(geminipart geminipartlib)
kcoreaddons_desktop_to_json(geminipart geminipart.desktop)

add_subdirectory(search)

install(TARGETS geminipart DESTINATION ${KDE_INSTALL_PLUGINDIR}/kf${QT_MAJOR_VERSION}/parts)
