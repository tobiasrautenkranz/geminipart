// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminibrowserextension.h"

#include "geminipart.h"
#include "geminiview.h"

#include "gemini-debug.h"

#include <QScrollBar>

#include <KActionCollection>
#include <KFileItem>
#include <KLocalizedString>
#include <KParts/BrowserRun>
#include <KStringHandler>
#include <KUriFilter>
#include <QApplication>
#include <QClipboard>
#include <QContextMenuEvent>
#include <QMimeData>
#include <QMimeDatabase>
#include <QTextBrowser>

GeminiBrowserExtension::GeminiBrowserExtension(GeminiPart *p)
    : KParts::BrowserExtension(p)
    , m_contextMenuActionCollection(new KActionCollection(this))
{
    setURLDropHandlingEnabled(true);
    Q_EMIT enableAction("copy", true);
    Q_EMIT enableAction("paste", true);

    auto view = part()->view();
    Q_ASSERT(view);
    connect(view, &QTextBrowser::anchorClicked, this, [&](const QUrl &link) {
        qCDebug(GEMINIPART) << "navigate" << link;
        Q_EMIT openUrlRequest(link);
    });

    connect(view, &GeminiView::linkMiddleClicked, this, [&](const QUrl &link) {
        qCDebug(GEMINIPART) << "new Tab" << link;
        KParts::BrowserArguments browserArguments;
        browserArguments.setNewTab(true);
        Q_EMIT openUrlRequest(link, KParts::OpenUrlArguments(), browserArguments);
    });

    connect(view, QOverload<const QUrl &>::of(&QTextBrowser::highlighted), this, [&](const QUrl &link) {
        KFileItem fileItem;
        if (link.isValid()) {
            fileItem = KFileItem(link, QString(), KFileItem::Unknown);
            Q_EMIT this->part()->setStatusBarText(link.toString());
        } else {
            Q_EMIT this->part()->setStatusBarText(QString());
        }
        Q_EMIT mouseOverInfo(fileItem);
    });

    connect(view, &GeminiView::contextMenuRequested, this, &GeminiBrowserExtension::requestContextMenu);
}

void GeminiBrowserExtension::requestContextMenu(const QPoint &pos, const QUrl &hlink, bool hasSelection)
{
    KParts::BrowserExtension::PopupFlags flags = KParts::BrowserExtension::DefaultPopupItems;
    flags |= KParts::BrowserExtension::ShowBookmark | KParts::BrowserExtension::ShowReload;

    KParts::OpenUrlArguments args;
    KParts::BrowserArguments browserArgs;

    qCDebug(GEMINIPART) << "context menu browser" << hasSelection;

    QUrl link = hlink;

    if (link.isValid()) {
        flags |= KParts::BrowserExtension::IsLink;
        flags |= KParts::BrowserExtension::ShowBookmark;
        flags |= KParts::BrowserExtension::ShowUrlOperations;

        QList<QAction *> linkActions;

        QAction *action = new QAction(i18n("Copy Link"), part());
        connect(action, &QAction::triggered, this, [link] {
            auto *data = new QMimeData;
            data->setUrls({link});
            QApplication::clipboard()->setMimeData(data, QClipboard::Clipboard);
        });
        m_contextMenuActionCollection->addAction(QStringLiteral("copylinkurl"), action);
        linkActions.append(action);

        action = new QAction(i18n("&Save Link As..."), this);
        m_contextMenuActionCollection->addAction(QStringLiteral("savelinkas"), action);
        connect(action, &QAction::triggered, this, [=](bool) {
            KParts::BrowserRun::saveUrl(link, link.path(), part()->view(), KParts::OpenUrlArguments());
        });
        linkActions.append(action);

        actionGroups.insert(QStringLiteral("linkactions"), linkActions);
    } else {
        link = part()->url();
        if (hasSelection) {
            flags = KParts::BrowserExtension::ShowTextSelectionItems;

            QList<QAction *> editActions;

            /*
            auto copy = KStandardAction::copy(part());
            copy->setText("&Copy Text");
            copy->setEnabled(true);
            connect(part()->view(), &QTextEdit::copyAvailable,
                    copy, &QAction::setEnabled);
            connect(copy, &QAction::triggered, part(), &GeminiPart::copySelection);

            editActions.append(copy);
            */

            auto text = part()->view()->textCursor().selectedText();
            KUriFilterData data(text);
            // data.setDefaultUrlScheme("gemini");

            QString searchProvider = QStringLiteral("geminispace");
            // QStringList alternateProviders;     alternateProviders << "gus" << "google" << "google_groups" << "google_news" << "webster" << "dmoz" <<
            // "wikipedia";
            // data.setAlternateSearchProviders(alternateProviders);
            data.setAlternateDefaultSearchProvider(searchProvider);

            // data.setSearchFilteringOptions(KUriFilterData::RetrievePreferredSearchProvidersOnly);
            qCDebug(GEMINIPART) << "providers" << data.preferredSearchProviders() << data.searchProvider();

            // bool filtered = KUriFilter::self()->filterSearchUri(data, KUriFilter::WebShortcutFilter);
            bool filtered = KUriFilter::self()->filterSearchUri(data, KUriFilter::NormalTextFilter);
            if (filtered) {
                auto search = new QAction(i18n("Search for '%1' with %2", KStringHandler::rsqueeze(text, 21), searchProvider), part());
                search->setData(QUrl(data.uri()));
                connect(search, &QAction::triggered, this, [&](bool) {
                    auto action = qobject_cast<QAction *>(sender());
                    if (!action)
                        return;

                    auto url = action->data().toUrl();
                    if (!url.isValid())
                        return;
                    qCDebug(GEMINIPART) << "search" << url;
                    KParts::BrowserArguments browserArgs;
                    browserArgs.frameName = QStringLiteral("_blank");
                    Q_EMIT openUrlRequest(url, KParts::OpenUrlArguments(), browserArgs);
                });
                // editActions.addAction("defaultSearchProvider", action);
                editActions.append(search);
            }

            actionGroups.insert(QStringLiteral("editactions"), editActions);

        } else {
            flags |= KParts::BrowserExtension::ShowNavigationItems;
        }
    }

    QString mimetype = QLatin1String("text/gemini");
    if (!link.isEmpty() && link.isLocalFile()) {
        QMimeDatabase db;
        mimetype = db.mimeTypeForUrl(link).name();
    }
    args.setMimeType(mimetype);

    Q_EMIT popupMenu(pos, link, static_cast<mode_t>(-1), args, browserArgs, flags, actionGroups);
}

void GeminiBrowserExtension::infoMessage(KJob *, const QString &plain, const QString &)
{
    qCDebug(GEMINIPART) << "infomessage" << plain;
    Q_EMIT BrowserExtension::infoMessage(plain);
}

void GeminiBrowserExtension::copy()
{
    part()->view()->copy();
}

int GeminiBrowserExtension::xOffset()
{
    return part()->view()->scrollPositionX();
}
int GeminiBrowserExtension::yOffset()
{
    return part()->view()->scrollPositionY();
}

GeminiPart *GeminiBrowserExtension::part() const
{
    return qobject_cast<GeminiPart *>(parent());
}

/*
   void GeminiBrowserExtension::printPreview()
   {
   QPrintPreviewDialog dialog(part()->view());
   connect(&dialog, &QPrintPreviewDialog::paintRequested, [=](QPrinter *p) {

   });
   }
   */

#include "moc_geminibrowserextension.cpp"
