// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminidocument.h"
#include "gemini-debug.h"
#include "parsestate.h"

#include <KParts/HistoryProvider>
#include <QChar>
#include <QDebug>
#include <QFontDatabase>
#include <QFontMetricsF>
#include <QPalette>
#include <QTextCursor>
#include <QTextList>
#include <QtGlobal>
#include <array>

static constexpr int headingMax = 3;

static bool inFont(const QFont font, uint c)
{
    return QFontMetricsF(font).inFontUcs4(c);
}

static void loadEmojiFont(QFont font)
{
    static const auto emojiFont = QLatin1String("Noto Color Emoji");
    const uint faceWithMonocleUcs4 = 0x0001F9D0;

    if (!inFont(font, faceWithMonocleUcs4)) {
        qCDebug(GEMINIPART) << "inserting Emoji substitution font " << emojiFont;
        QFont::insertSubstitution(font.family(), emojiFont);
        if (!inFont(QFont(font.family()), faceWithMonocleUcs4)) {
            qCWarning(GEMINIPART) << "Emoji font substitution failed";
        }
    }
}

/** The format to be applied for the different types of Gemini text lines. */
struct Formats {
    struct EmojiFontLoader {
        static QStringList fontFamilies()
        {
            QStringList f;
            f.append(QTextCharFormat().font().family());
            f.append(QFontDatabase::systemFont(QFontDatabase::FixedFont).family());

            f.removeDuplicates();
            return f;
        }

        /** loads Emoji Fonts. Needs to be called before constructing QFont Objects.*/
        EmojiFontLoader()
        {
            const auto fonts = fontFamilies();
            for (const auto &fontFamily : fonts) {
                loadEmojiFont(fontFamily);
            }
        }
    } emojiFonts;

    Formats()
    {
        link.setAnchor(true);

        QPalette palette;
        link.setForeground(palette.link());

        // update default font for emoji substitution
        text.setFontWeight(QFont::Normal);
        link.setFontWeight(QFont::Normal);

        textBlock.setBottomMargin(5);

        linkVisited = QTextCharFormat(link); // create copy
        linkVisited.setForeground(palette.linkVisited());

        codeText.setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));

        codeBlock.setProperty(QTextFormat::BlockCodeFence, '`');
        // needs to be set for markdown export to work, otherwise two newlines
        // are emitted for every line.
        codeBlock.setProperty(QTextFormat::BlockCodeLanguage, QLatin1String(""));
        codeBlock.setNonBreakableLines(true);
        codeBlock.setTextIndent(0);

        constexpr auto blockQuoteTopLevel = 1;
        quoteBlock.setProperty(QTextFormat::BlockQuoteLevel, blockQuoteTopLevel);
        quoteBlock.setIndent(1);
        // needs to be set for toMarkdown() to output '>'
        // Use anyway for styling

        std::array<qreal, headingMax> margins = {20, 10, 5};
        for (size_t i = 0; i < margins.size(); i++) {
            headingBlock[i].setHeadingLevel(static_cast<int>(i));
            headingBlock[i].setTopMargin(margins[i]);
            headingBlock[i].setBottomMargin(margins[i] / 2);

            headingText[i].setFontWeight(QFont::Bold);
            headingText[i].setProperty(QTextFormat::FontSizeAdjustment, headingMax - static_cast<int>(i));
        }
    }
    /** default block format */
    QTextBlockFormat block;

    QTextCharFormat link;
    QTextCharFormat linkVisited;

    QTextCharFormat text;
    QTextBlockFormat textBlock;

    QTextCharFormat codeText;
    QTextBlockFormat codeBlock;

    QTextCharFormat quoteText;
    QTextBlockFormat quoteBlock;

    std::array<QTextCharFormat, headingMax> headingText;
    std::array<QTextBlockFormat, headingMax> headingBlock;
};

Q_GLOBAL_STATIC(const Formats, formats);

GeminiDocument::GeminiDocument(QObject *parent)
    : QTextDocument(parent)
{
    setIndentWidth(20);
    setDocumentMargin(20);
    Q_ASSERT(mCursor.document());
}

const QString &GeminiDocument::source() const
{
    return mSource;
}

// An empty QTextDocument has one block, that can not be deleted. Use it when
// empty such that no leading newline is in the document.
static void insertBlock(QTextCursor &cursor, const QTextBlockFormat &format, const QTextCharFormat &charFormat)
{
    Q_ASSERT(cursor.document());
    if (!cursor.document()->isEmpty()) {
        cursor.insertBlock();
    }

    cursor.setBlockFormat(format);
    cursor.setBlockCharFormat(charFormat);
}

void GeminiDocument::appendSourceLine(const QStringRef &line)
{
    const char c1 = line.size() >= 1 ? line[0].toLatin1() : '0';

    if (line.startsWith(QLatin1String("```"))) {
        if (mParseState == ParseState::PREFORMAT) {
            // extra newline at end of preformat block
            insertBlock(mCursor, formats->codeBlock, formats->codeText);
            mParseState = ParseState::DEFAULT;
        } else {
            mParseState = ParseState::PREFORMAT;
        }
    } else if (mParseState == ParseState::PREFORMAT) {
        insertBlock(mCursor, formats->codeBlock, formats->codeText);
        mCursor.insertText(line.toString());
    } else {
        mParseState = ParseState::DEFAULT;

        if ('#' == c1) {
            int level = 0;
            for (int c = 1; c < headingMax; c++) {
                if (c < line.size() && line[c] == QLatin1Char('#')) {
                    level++;
                }
            }
            Q_ASSERT(level < headingMax);
            const bool isFirstLine = isEmpty();
            insertBlock(mCursor, formats->headingBlock[level], formats->headingText[level]);

            auto text = line.mid(mWhiteSpaceOptionalRegEx.match(line, level + 1).capturedEnd()).toString();
            mCursor.insertText(text);

            if (isFirstLine && 0 == level && !text.isEmpty()) {
                setMetaInformation(MetaInformation::DocumentTitle, text);
                Q_EMIT titleChanged(text);
            }
        } else if ('*' == c1 && line.size() > 2 && line[1] == QLatin1Char(' ')) {
            insertBlock(mCursor, formats->block, formats->text);
            mCursor.insertText(line.mid(2).toString());
            Q_ASSERT(mCursor.document());
            Q_ASSERT(mCursor.block().document());
            if (!mCursor.currentList()) {
                mCursor.createList(QTextListFormat::ListDisc);
            } else {
                mCursor.currentList()->add(mCursor.block());
            }
        } else if ('>' == c1) {
            insertBlock(mCursor, formats->quoteBlock, formats->quoteText);
            mCursor.insertText(line.mid(1).toString());
        } else if ('=' == c1 && line.size() > 3 && line[1] == QLatin1Char('>')) {
            int start = mWhiteSpaceOptionalRegEx.match(line, 2).capturedEnd();
            const auto sep = mWhiteSpaceRegEx.match(line, start);

            const auto urlString = line.mid(start, sep.capturedStart() - start).toString();
            QUrl url(urlString);
            if (url.isRelative()) {
                url = mUrl.resolved(url);
            }
            auto link = formats->link;
            if (url.isValid()) {
                // Styling visited links in QTextBrowser is not supported; do it
                // here
                if (KParts::HistoryProvider::self()->contains(url.toString())) {
                    link = formats->linkVisited;
                }
                link.setAnchorHref(url.toString());
            }

            insertBlock(mCursor, formats->block, link);
            if (sep.hasMatch() && sep.capturedEnd() < line.size() - 1) {
                mCursor.insertText(line.mid(sep.capturedEnd()).toString());
            } else {
                // FIXME QUrl::toString() may be different from urlString; should we generate an error?
                mCursor.insertText(urlString);
            }
        } else {
            insertBlock(mCursor, formats->textBlock, formats->text);
            mCursor.insertText(line.toString());
        }
    }
}

void GeminiDocument::appendSource(const QString &source)
{
    qCDebug(GEMINIPART) << "appendSource" << source;
    assert(mUrl.isValid());
    assert(!mCursor.isNull());

    mSource += source;

    static const QRegularExpression newLine(QLatin1String("\r\n|\n"));
    for (auto i = mSourceCurrentLine; i < mSource.size();) {
        int lineEnd = mSource.indexOf(newLine, i);

        assert(-1 == lineEnd || lineEnd - i >= 0);
        auto line = lineEnd >= 0 ? mSource.midRef(i, lineEnd - i) : mSource.midRef(i);
        qCDebug(GEMINIPART) << "processing line" << i << " " << lineEnd << " " << line;

        if (-1 == lineEnd) {
            mSourceCurrentLine = i;
            return;
        }

        appendSourceLine(line);

        i = lineEnd + (mSource[lineEnd] == QLatin1Char('\r') ? 2 : 1);
        mSourceCurrentLine = i;
    }
}

void GeminiDocument::endAppendSource()
{
    if (mSourceCurrentLine < mSource.size()) {
        appendSourceLine(mSource.midRef(mSourceCurrentLine));

        mSourceCurrentLine = mSource.size();
    }
}

void GeminiDocument::setUrl(const QUrl &url)
{
    mUrl = url;
}

void GeminiDocument::setSource(const QString &source, QUrl url)
{
    clear();
    setUrl(url);
    appendSource(source);
    endAppendSource();
}

void GeminiDocument::clear()
{
    setMetaInformation(MetaInformation::DocumentTitle, QString());
    mSource.clear();
    mUrl.clear();
    QTextDocument::clear();
    mSourceCurrentLine = 0;
    mParseState = ParseState::DEFAULT;
    mCursor = QTextCursor(this);
}

#include "moc_geminidocument.cpp"
