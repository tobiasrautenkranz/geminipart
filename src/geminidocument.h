// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include "parsestate.h"
#include <QRegularExpression>
#include <QTextCursor>
#include <QTextDocument>

class QTextList;

/** Parses text/gemini */
class GeminiDocument : public QTextDocument
{
    Q_OBJECT

public:
    GeminiDocument(QObject *parent = nullptr);

    /** Appends source text and parses it.
     * Previously added text is taken into account.
     * You need to call endAppendSource() after all source data has been added. */
    void appendSource(const QString &source);

    /** Call to indicate that the end of the document has been reached. */
    void endAppendSource();

    /** Load a gemini document. */
    void setSource(const QString &source, QUrl url);

    void clear() override;

    /** Sets the base url used to resolve relative link urls */
    void setUrl(const QUrl &url);

    /** Returns the document source */
    const QString &source() const;

Q_SIGNALS:
    /** Emitted when a document title is found */
    void titleChanged(QString title);

protected:
    /** The gemini source code of the document */
    QString mSource;

    /** Offset in the source where current line to be processed begins.
     * i.e.: the char after the last newline character. */
    int mSourceCurrentLine = 0;

    /** The URL of the document location */
    QUrl mUrl;

    /** The active cursor of the document */
    QTextCursor mCursor{this};

    /** At least one whitespace character */
    const QRegularExpression mWhiteSpaceRegEx{QLatin1String("\\s+")};

    /** Zero or more whitespace characters */
    const QRegularExpression mWhiteSpaceOptionalRegEx{QLatin1String("\\s*")};

    /** The current state of the parser.
     * Preformat or not */
    ParseState mParseState = ParseState::DEFAULT;

    /** Parses the source line and appends it to the document */
    void appendSourceLine(const QStringRef &line);
};
