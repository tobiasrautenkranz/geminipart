// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminipart.h"

#include "geminibrowserextension.h"
#include "geminidocument.h"
#include "geminitextextension.h"
#include "geminiview.h"
#include "scopedtextcursorrestore.h"

#include "gemini-debug.h"

#include <KAboutData>
#include <KActionCollection>
#include <KIO/JobUiDelegate>
#include <KIO/JobUiDelegateFactory>
#include <KIO/OpenUrlJob>
#include <KIO/TransferJob>
#include <KLocalizedString>
#include <KParts/BrowserRun>
#include <KParts/GUIActivateEvent>
#include <KParts/OpenUrlArguments>
#include <KPluginMetaData>
#include <QApplication>
#include <QFile>
#include <QFileDialog>
#include <QInputDialog>
#include <QTextDecoder>
#include <ksslinfodialog.h>

enum PageSecurity { PageUnencrypted, PageEncrypted, PageMixed };

GeminiPart::GeminiPart(QWidget *parentWidget, QObject *parent, const KPluginMetaData &metaData, const QVariantList &)
    : KParts::ReadOnlyPart(parent)
    , mDecode()
{
    setMetaData(metaData);

    setWidget(new GeminiView(parentWidget));
    view()->setDocument(new GeminiDocument(this));
    view()->setObjectName(QStringLiteral("geminipart"));

    new GeminiBrowserExtension(this);
    new GeminiTextExtension(this);

    mCollection = new KActionCollection(view());

    initActions();

    connect(view(), &QTextEdit::copyAvailable, this, &GeminiPart::copyAvailable);
    connect(document(), &GeminiDocument::titleChanged, this, &KParts::Part::setWindowCaption);
    connect(document(), &GeminiDocument::titleChanged, view(), &QTextEdit::setDocumentTitle);

    setXMLFile(QLatin1String("geminipart.rc"));
}

GeminiView *GeminiPart::view()
{
    return qobject_cast<GeminiView *>(widget());
}

GeminiDocument *GeminiPart::document()
{
    return qobject_cast<GeminiDocument *>(view()->document());
}

GeminiPart::~GeminiPart()
{
    if (mJob) {
        mJob->kill();
    }
}
void GeminiPart::begin(const QUrl &url)
{
    setUrl(url);
    Q_EMIT setWindowCaption(url.toDisplayString());

    document()->clear();
    document()->setUrl(url);

    if (mJob) {
        mJob->kill();
    }
}

void GeminiPart::end()
{
    restoreScrollPosition();
    Q_EMIT completed();
}

bool GeminiPart::openUrl(const QUrl &url)
{
    qCDebug(GEMINIPART) << "openurl " << url;
    if (!url.isValid()) {
        return false;
    }
    begin(url);

    /*
       if (url.isLocalFile()) {
       setLocalFilePath(url.url());
       return openFile();
       }
       */

    mJob = KIO::get(url, arguments().reload() ? KIO::Reload : KIO::NoReload, KIO::HideProgressInfo);
    mJob->addMetaData(QLatin1String("inputSupported"), QLatin1String("true"));
    qCDebug(GEMINIPART) << "set outgoing metadata" << mJob->outgoingMetaData();
    connect(mJob, &KIO::TransferJob::mimeTypeFound, this, &GeminiPart::mimeType);
    connect(mJob, &KIO::TransferJob::data, this, &GeminiPart::data);
    connect(mJob, &KIO::Job::finished, this, &GeminiPart::jobFinished);
    connect(mJob, &KIO::Job::result, this, &GeminiPart::result);
    connect(mJob, &KJob::infoMessage, qobject_cast<GeminiBrowserExtension *>(browserExtension()), &GeminiBrowserExtension::infoMessage);
    mJob->setUiDelegate(KIO::createDefaultJobUiDelegate(KJobUiDelegate::AutoHandlingEnabled, view()));
    mJob->start();

    Q_EMIT started(mJob);

    return true;
}

void GeminiPart::jobFinished(KJob *job)
{
    if (mJob) {
        Q_ASSERT(job == mJob);
        mJob = nullptr;
    }
}

void GeminiPart::restoreScrollPosition()
{
    const KParts::OpenUrlArguments args(arguments());
    view()->setScrollPosition({args.xOffset(), args.yOffset()});
    qCDebug(GEMINIPART) << "scroll" << browserExtension()->xOffset() << " " << browserExtension()->yOffset();
}

bool GeminiPart::closeUrl()
{
    if (mJob) {
        mJob->kill();
    }

    return ReadOnlyPart::closeUrl();
}

void GeminiPart::data(KIO::Job *, const QByteArray &data)
{
    ScopedTextCursorRestore cursor(*view());

    document()->appendSource(mDecode->toUnicode(data));
}

void GeminiPart::result(KJob *kjob)
{
    qCDebug(GEMINIPART) << "loading done";

    auto job = qobject_cast<KIO::Job *>(kjob);
    if (job && !job->queryMetaData(QLatin1String("input")).isNull()) {
        const auto prompt = job->queryMetaData(QLatin1String("input"));
        const auto text = QInputDialog::getText(view(), QLatin1String("Gemini Prompt"), prompt);
        if (!text.isEmpty()) {
            auto url = this->url();
            url.setQuery(text);
            openUrl(url);
        }
        return;
    }
    end();
}

void GeminiPart::mimeType(KIO::Job *job, const QString & /*mimeType*/)
{
    mMetaData = job->metaData();

    qCDebug(GEMINIPART) << "meta" << job->metaData();
    auto charset = job->metaData().value(QLatin1String("charset"), QLatin1String("UTF-8")).toUtf8();
    auto codec = QTextCodec::codecForName(charset);
    qCDebug(GEMINIPART) << "charset: " << charset;
    if (codec) {
        mDecode.reset(codec->makeDecoder());
    } else {
        auto c = QTextCodec::QTextCodec::codecForName("UTF-8");
        Q_ASSERT(c);
        mDecode.reset(c->makeDecoder());
    }

    Q_EMIT browserExtension()->setPageSecurity(PageEncrypted);
}

void GeminiPart::saveDocument()
{
    QString name = url().fileName().isEmpty() ? QLatin1String("index.gmi") : url().fileName();

    QFileDialog dialog(view(), QLatin1String("Save As"));
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setMimeTypeFilters({QLatin1String("text/gemini")});
    dialog.selectFile(name);

    if (dialog.exec() == QDialog::Accepted) {
        auto destUrl = dialog.selectedUrls().value(0);
        KParts::BrowserRun::saveUrlUsingKIO(url(), destUrl, view(), {});
    }
}

void GeminiPart::initActions()
{
    actionCollection()->addAction(KStandardAction::SaveAs, QLatin1String("saveDocument"), this, SLOT(saveDocument()));

    QAction *action = new QAction(QIcon::fromTheme(QStringLiteral("zoom-in")), i18nc("zoom in action", "Zoom In"), this);
    actionCollection()->addAction(QStringLiteral("zoomIn"), action);
    actionCollection()->setDefaultShortcuts(action, QList<QKeySequence>() << QKeySequence(QStringLiteral("CTRL++")) << QKeySequence(QStringLiteral("CTRL+=")));
    connect(action, &QAction::triggered, view(), &GeminiView::zoomIn);

    action = new QAction(QIcon::fromTheme(QStringLiteral("zoom-out")), i18nc("zoom out action", "Zoom Out"), this);
    actionCollection()->addAction(QStringLiteral("zoomOut"), action);
    actionCollection()->setDefaultShortcuts(action, QList<QKeySequence>() << QKeySequence(QStringLiteral("CTRL+-")) << QKeySequence(QStringLiteral("CTRL+_")));
    connect(action, &QAction::triggered, view(), &GeminiView::zoomOut);

    action = new QAction(QIcon::fromTheme(QStringLiteral("zoom-original")), i18nc("reset zoom action", "Actual Size"), this);
    actionCollection()->addAction(QStringLiteral("zoomNormal"), action);
    actionCollection()->setDefaultShortcut(action, QKeySequence(QStringLiteral("CTRL+0")));
    connect(action, &QAction::triggered, view(), &GeminiView::resetZoom);

    action = new QAction(i18n("View Do&cument Source"), this);
    actionCollection()->addAction(QStringLiteral("viewDocumentSource"), action);
    actionCollection()->setDefaultShortcut(action, QKeySequence(Qt::CTRL | Qt::Key_U));
    connect(action, &QAction::triggered, this, [&]() {
        KIO::OpenUrlJob *job = new KIO::OpenUrlJob(url(), QStringLiteral("text/plain"));
        job->setUiDelegate(KIO::createDefaultJobUiDelegate(KJobUiDelegate::AutoHandlingEnabled, view()));
        job->start();
    });

    action = new QAction(QStringLiteral("SSL"), this);
    actionCollection()->addAction(QStringLiteral("security"), action);
    connect(action, &QAction::triggered, this, &GeminiPart::showSecurity);

    // action = KStandardAction::create(KStandardAction::Find, this,
    //    &WebEnginePart::slotShowSearchBar, actionCollection());
    //
    //  action = new QAction(QIcon::fromTheme(QStringLiteral("document-print-preview")), i18n("Print Preview"), this);
    //   actionCollection()->addAction(QStringLiteral("printPreview"), action);
    //   connect(action, &QAction::triggered, browserExtension(), &WebEngineBrowserExtension::slotPrintPreview);
}

void GeminiPart::showSecurity()
{
    qCDebug(GEMINIPART) << "ssl: " << mMetaData;
    KSslInfoDialog *kid = new KSslInfoDialog(nullptr);

    const QStringList sl = mMetaData[QLatin1String("ssl_peer_chain")].split(QLatin1Char('\x01'), Qt::SkipEmptyParts);
    QList<QSslCertificate> certChain;
    for (const QString &s : sl) {
        certChain.append(QSslCertificate(s.toLatin1()));
    }

    kid->setSslInfo(certChain,
                    mMetaData[QLatin1String("ssl_peer_ip")],
                    this->url().host(),
                    mMetaData[QLatin1String("ssl_protocol_version")],
                    mMetaData[QLatin1String("ssl_chipher")],
                    mMetaData[QLatin1String("ssl_cipher_used_bits")].toInt(),
                    mMetaData[QLatin1String("ssl_cipher_bits")].toInt(),
                    KSslInfoDialog::certificateErrorsFromString(mMetaData[QLatin1String("ssl_cert_errors")]));

    kid->exec();
}

void GeminiPart::copyAvailable(bool yes)
{
    Q_EMIT browserExtension()->enableAction("copy", yes);
}

void GeminiPart::copySelection()
{
    view()->copy();
}

void GeminiPart::guiActivateEvent(KParts::GUIActivateEvent *event)
{
    if (event && event->activated()) {
        if (!view()->documentTitle().isEmpty()) {
            Q_EMIT setWindowCaption(view()->documentTitle());
        } else if (!url().isEmpty()) {
            Q_EMIT setWindowCaption(url().toDisplayString());
        } else {
            Q_EMIT setWindowCaption(QString());
        }
    }
}

bool GeminiPart::doOpenStream(const QString &mimeType)
{
    if (mimeType == QLatin1String("text/gemini")) {
        begin(url());

        auto codec = QTextCodec::QTextCodec::codecForName("UTF-8");
        Q_ASSERT(codec);
        mDecode.reset(codec->makeDecoder());

        Q_EMIT started(nullptr);

        return true;
    }
    return false;
}
bool GeminiPart::doWriteStream(const QByteArray &d)
{
    data(nullptr, d);
    return true;
}
bool GeminiPart::doCloseStream()
{
    ScopedTextCursorRestore cursor(*view());

    document()->endAppendSource();

    return true;
}

#include "moc_geminipart.cpp"
