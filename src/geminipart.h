// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <KIO/MetaData>
#include <KParts/ReadOnlyPart>
#include <QLoggingCategory>
#include <QScopedPointer>
#include <kparts_version.h>

class GeminiView;
class QTextDocument;
class GeminiBrowserExtension;
class GeminiDocument;

Q_DECLARE_LOGGING_CATEGORY(KGEMINIPART)

namespace KIO
{
class TransferJob;
}
namespace KParts
{
class GUIActivateEvent;
}

/** Gemini text file viewer KPart plugin.  */
class GeminiPart : public KParts::ReadOnlyPart
{
    Q_OBJECT

public:
    GeminiPart(QWidget *parentWidget, QObject *parent, const KPluginMetaData &metaData, const QVariantList &args);

    ~GeminiPart() override;

    /** Returns the view widget that is displaying the document */
    GeminiView *view();
    /** Returns the document that is being displayed */
    GeminiDocument *document();

    // bool openFile() override;
    bool openUrl(const QUrl &url) override;

    void copySelection();

    bool closeUrl() override;

protected:
    bool doOpenStream(const QString &mimeType) override;
    bool doWriteStream(const QByteArray &dat) override;
    bool doCloseStream() override;

    void guiActivateEvent(KParts::GUIActivateEvent *event) override;

    /** start loading from url */
    void begin(const QUrl &url);
    void end();

    /** initialize the menu actions */
    void initActions();

    void restoreScrollPosition();

    QScopedPointer<QTextDecoder> mDecode;

    KIO::TransferJob *mJob = nullptr;

    KActionCollection *mCollection;
    KIO::MetaData mMetaData;

protected Q_SLOTS:
    void data(KIO::Job *, const QByteArray &);
    void mimeType(KIO::Job *, const QString &mimeType);
    void jobFinished(KJob *);
    void result(KJob *);
    void showSecurity();
    void saveDocument();

    void copyAvailable(bool);
};
