// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminipart.h"

#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON(GeminiPart, "geminipart.json")

#include "geminipartfactory.moc"
