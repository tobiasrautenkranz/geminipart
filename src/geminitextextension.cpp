// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminitextextension.h"

#include "geminipart.h"
#include "geminiview.h"

#include "gemini-debug.h"

#include <QTextDocumentFragment>

GeminiTextExtension::GeminiTextExtension(GeminiPart *part)
    : KParts::TextExtension(part)
{
}

bool GeminiTextExtension::hasSelection() const
{
    return part()->view()->textCursor().hasSelection();
}

QString GeminiTextExtension::completeText(TextExtension::Format format) const
{
    switch (format) {
    case TextExtension::PlainText:
        return part()->view()->document()->toPlainText();
    case TextExtension::HTML:
        return part()->view()->document()->toHtml();
    }
    return {};
}

QString GeminiTextExtension::selectedText(TextExtension::Format format) const
{
    switch (format) {
    case TextExtension::PlainText:
        return part()->view()->textCursor().selectedText();
    case TextExtension::HTML:
        return part()->view()->textCursor().selection().toHtml();
    }
    return {};
}

bool GeminiTextExtension::findText(const QString &string, KFind::SearchOptions options) const
{
    if (string.isEmpty()) {
        return false;
    }

    QTextDocument::FindFlags flags;
    if (options.testFlag(KFind::FindBackwards))
        flags |= QTextDocument::FindBackward;
    if (options.testFlag(KFind::CaseSensitive))
        flags |= QTextDocument::FindCaseSensitively;
    if (options.testFlag(KFind::WholeWordsOnly))
        flags |= QTextDocument::FindWholeWords;

    auto view = part()->view();

    auto cursor = view->document()->find(string, view->textCursor(), flags);

    if (cursor.isNull()) {
        return false;
    }

    view->setTextCursor(cursor);
    return true;
}

GeminiPart *GeminiTextExtension::part() const
{
    return qobject_cast<GeminiPart *>(parent());
}

#include "moc_geminitextextension.cpp"
