// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <KParts/TextExtension>

class KActionCollection;
class GeminiPart;

/** TextExtension Plugin for GeminiPart. Use by Konqueror for searching the
 * document text using the searchbar. */
class GeminiTextExtension : public KParts::TextExtension
{
    Q_OBJECT

public:
    explicit GeminiTextExtension(GeminiPart *part);

    bool hasSelection() const override;
    QString completeText(TextExtension::Format format) const override;
    QString selectedText(TextExtension::Format format) const override;

    bool findText(const QString &string, KFind::SearchOptions options) const override;

private:
    GeminiPart *part() const;
};
