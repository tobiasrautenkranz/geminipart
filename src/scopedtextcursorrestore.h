// SPDX-FileCopyrightText: 2024 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QTextEdit>

/** Save the cursor postition of a QTextEdit on construction and restores it on destruction */
class ScopedTextCursorRestore {
    public:
        ScopedTextCursorRestore(QTextEdit& browser)
            : mEdit(browser),
              mPosition(browser.textCursor().position())
        {}

        /** restore the cursor position */
        ~ScopedTextCursorRestore()
        {
            QTextCursor cursor = mEdit.textCursor();
            cursor.setPosition(mPosition);
            mEdit.setTextCursor(cursor);
        }

    private:
        QTextEdit& mEdit;
        const int mPosition;
};
