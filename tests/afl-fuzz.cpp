// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminidocument.h"

#include <QApplication>
#include <QStringLiteral>
#include <QTextDecoder>
#include <QUrl>
#include <unistd.h> // needed for AFL; call to read()

__AFL_FUZZ_INIT();

/** Fuzz multiple appendSource calls.
 *
 * The first char of the buffer is the offset where to split the document
 * for the two appendSource() calls. The rest is a utf8 encoded gemini document.
 */
int main(int argc, char *argv[])
{
    // QApplication is need for the QFont usage in GeminiDocument
    QApplication app(argc, argv);

#ifdef __AFL_HAVE_MANUAL_CONTROL
    __AFL_INIT();
#endif
    const unsigned char *buf = __AFL_FUZZ_TESTCASE_BUF;
    const QUrl url{QStringLiteral("foo")};

    GeminiDocument document;

    while (__AFL_LOOP(UINT_MAX)) {
        int len = __AFL_FUZZ_TESTCASE_LEN;

        document.clear();
        document.setUrl(url);

        if (len < 2) {
            continue;
        }

        // offset where to split the document in two
        const auto mid = (int)buf[0];

        // Gemini document source
        const auto source = QString::fromUtf8(reinterpret_cast<const char *>(buf) + 1, len - 1);

        auto part1Len = std::min(mid, source.size());
        document.appendSource(source.left(part1Len));
        auto part2Len = source.size() - part1Len;
        if (part2Len > 0) {
            document.appendSource(source.right(part2Len));
        }
        document.endAppendSource();

        auto mdFromParts = document.toMarkdown();
        document.setSource(source, url);

        // Check that progressive loading with appendSource gives the same
        // result as loading the document all at once.
        if (mdFromParts != document.toMarkdown()) {
            abort();
        }
    }

    return 0;
}
